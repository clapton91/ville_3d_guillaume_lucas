# Ville_3D_guillaume_lucas

# What's in there ?

Welcome in our city modelling project. We offer you to wander in a futuristic post-apocalyptic world we created using Blender, 
and that we animated using OpenGL. 

The following features have been implemented: 

1) A day to night cycle, whose duration is parametrizable within the code of the main.cpp file (variable name is cycle_duration) . 
It proposes by default 20s of day and 20s of night.

2) A flying car running in the air along a pre-defined route. 

3) A 3-mode camera : pedestrian, aerial and free. Use q,z,s,d keys to move and the mouse to change the direction of the view, and press the space bar to switch between the three camera modes.

4) The random generation of our world from the tiles we created under Blender, following some consistency constraints implemented in the code of the main.cpp

# How do to launch it ?

In Ubuntu terminal : 

1) git clone https://gitlab.com/clapton91/ville_3d_guillaume_lucas.git

Important: Since the tiles we exported from Blender are quite heavy, it may take some time to clone the project.

2) cd ville_3d_guillaume_lucas

3) cmake CMakeListst.txt

4) make

5) ./main

Important: Because of the size of our models, the window will typically takes 1 minutes to set up. Please ignore the error message coming from the opengl window.

Have fun ! 
