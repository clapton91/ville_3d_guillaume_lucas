#ifndef FLYINGCAR_H
#define FLYINGCAR_H
#include <vector>
#include <stdlib.h>     /* abs */
#include <random>
#include <iostream>

class FlyingCar
{
public:
    FlyingCar(int row, int column, int tile, std::vector<std::vector<std::vector<bool>>> _terrain){
        std::vector<int> _position;
        _position.push_back(row);
        _position.push_back(column);
        _position.push_back(tile); //car position in a tile (Up, Left, Right, Down)
        position = _position;
        terrain = _terrain;
    }

    std::vector<int> position;
    std::vector<std::vector<std::vector<bool>>> terrain;

    void move();

};

#endif // FLYINGCAR_H
