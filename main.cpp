#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <flyingcar.h>

// GLEW
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>

// GLM Mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Shader loading class
#include "Shader.h"
// Camera loading class
#include "RealisticCamera.h"
//#include "Camera.h"
// Model loading class
#include "Model.h"

// Other Libs
#include <SOIL.h>

// Dimension of the window
const GLuint WIDTH = 800, HEIGHT = 600;

// Main function, it is here that we initialize what is relative to the context
// of OpenGL and that we launch the main loop
int main()
{
    std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
    // Initialization of GLFW
    glfwInit();

    // Specify the OpenGL version to be used (3.3)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //.3
    // Disable the deprecated functions
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    // Necesary for Mac OS 
    #ifdef __APPLE__
      glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    #endif
    // Prevent the change of dimension of the window
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    
    // Create the application window
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "MyWindow", nullptr, nullptr);    
    if (window == nullptr)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    
    // Associate the created window with the OpenGL context
    glfwMakeContextCurrent(window);
    // Associate the callback with the pressure of a key on the keyboard
    glfwSetKeyCallback(window, key_callback);

    // Variable global that allows to ask the GLEW library to find the modern 
    // functions of OpenGL
    glewExperimental = GL_TRUE;
    // Initializing GLEW to retrieve pointers from OpenGL functions
    if (glewInit() != GLEW_OK)
    {
        std::cout << "Failed to initialize GLEW" << std::endl;
        return -1;
    }    

    // Transform the window dimensions to the dimensions of OpenGL (between -1 and 1)
    int width, height;
    // Recover the dimensions of the window created above
    glfwGetFramebufferSize(window, &width, &height); 
    glViewport(0, 0, width, height);

    // Allow to test the depth information
    glEnable(GL_DEPTH_TEST);
    
    // Build and compile our vertex and fragment shaders
    Shader shaders("shaders/c3/default.vertexshader", 
        "shaders/c3/default.fragmentshader");
    
    // Declare the positions and uv coordinates in the same buffer
    GLfloat vertices[] = {
        // Positions       // Colors         // normals
         500.5f,  0.0f,  500.5f, 1.0f, 0.0f, 0.0f, //500.0f, 500.0f, // Top right point
         500.5f,  0.0f, -500.5f, 0.0f, 1.0f, 0.0f, //500.0f, 0.0f, // Bottom right point
        -500.5f,  0.0f, -500.5f, 0.0f, 0.0f, 0.0f, //0.0f, 0.0f, // Bottom left point
        -500.5f,  0.0f,  500.5f, 1.0f, 1.0f, 0.0f, //0.0f, 500.0f, // Top left point
    };

    // The table of indices to rebuild our triangles
    GLushort indices[] = {  
        0, 1, 3,   // First triangle
        1, 2, 3    // Second triangle
    }; 
    
    // Declare the identifiers of our VAO, VBO and EBO
    GLuint VAO, VBO, EBO;
    // Inform OpenGL to generate one VAO
    glGenVertexArrays(1, &VAO);
    // Inform OpenGL to generate one VBO
    glGenBuffers(1, &VBO);
    // Inform OpenGL to generate one EBO
    glGenBuffers(1, &EBO); 
    
    // Bind the VAO
    glBindVertexArray(VAO);
    // Bind and fill the VBO
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    // Bind and fill the EBO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);
    // Color attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), 
        (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);
    // UV coordinates
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), 
        (GLvoid*)(6 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);
    
    // VBO is detached from the current buffer in the OpenGL context
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // VAO is detached from the current object in the OpenGL context
    glBindVertexArray(0);
    // EBO is detached (the last one!)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); 


    // Camera
    RealisticCamera camera = RealisticCamera(glm::vec3(0.0f,10.0f, 0.0f), window);

    //RealisticCamera camera = RealisticCamera(glm::vec3(0.0f, 0.0f, 3.0f), window);

    //Load a model
    Model car_up((GLchar*)"model/Flying_car/flying_car_up.obj");
    Model car_left((GLchar*)"model/Flying_car/flying_car_left.obj");
    Model car_right((GLchar*)"model/Flying_car/flying_car_right.obj");
    Model car_down((GLchar*)"model/Flying_car/flying_car_down.obj");
    Model Tile_1101((GLchar*)"model/building/Tile_1101.obj");
    Model Tile_1111((GLchar*)"model/building/Tile_1111.obj");
    Model Tile_1110((GLchar*)"model/building/Tile_1110.obj");
    Model Tile_1001((GLchar*)"model/building/Tile_1001.obj");
    Model Tile_0111((GLchar*)"model/building/Tile_0111.obj");
    Model Tile_0011((GLchar*)"model/building/Tile_0011.obj");
    Model Tile_0001((GLchar*)"model/building/Tile_0001.obj");
    Model Tile_1100((GLchar*)"model/building/Tile_1100.obj");
    Model Tile_0110((GLchar*)"model/building/Tile_0110.obj");
    Model Tile_1011((GLchar*)"model/building/Tile_1011.obj");
    Model Tile_1010((GLchar*)"model/building/Tile_1010.obj");
    Model Tile_0101((GLchar*)"model/building/Tile_0101.obj");
    Model Tile_1000((GLchar*)"model/building/Tile_1000.obj");
    Model Tile_0100((GLchar*)"model/building/Tile_0100.obj");
    Model Tile_0010((GLchar*)"model/building/Tile_0010.obj");
    Model Tile_0000((GLchar*)"model/building/Tile_0000.obj");



    srand(clock());
    std::vector<std::vector<std::vector<bool>>> matrice_terrain;
    int ligne = 3;
    int colonne = 3;
    for(int i = 0; i<ligne; i++){
        std::vector<std::vector<bool>> matrice_colonne;
        for(int j = 0; j<colonne; j++){
            std::vector<bool> matrice_case;
            int counter_openpath = 0;
            //Up, Left, Right, Down
            for(int k = 0; k<4; k++){
                int random = rand()%2;
                //If Up
                if(k==0){
                    //If up border
                    if(i == 0){
                        matrice_case.push_back(false);
                    }
                    else{
                        // If the down path of the case upside is open
                        if(matrice_terrain.at(i-1).at(j).at(3)){
                            matrice_case.push_back(true);
                            counter_openpath += 1;
                        }
                        else{
                            matrice_case.push_back(false);
                        }
                    }
                }
                //If Left
                else if(k==1){
                    //If left border
                    if(j == 0){
                        matrice_case.push_back(false);
                    }
                    else{
                        // If the right path of the case at the left of the current one is open
                        if(matrice_colonne.at(j-1).at(2)){
                            matrice_case.push_back(true);
                            counter_openpath += 1;
                        }
                        else{
                            matrice_case.push_back(false);
                        }
                    }
                }
                //If Right
                else if(k==2){
                    //If right border
                    if(j == colonne-1){
                        matrice_case.push_back(false);
                    }
                    else{
                        //We need at least two open paths
                        if(counter_openpath == 0){
                            matrice_case.push_back(true);
                            counter_openpath += 1;
                        }
                        else{
                            matrice_case.push_back((bool)random);
                            counter_openpath += random;
                        }
                    }
                }
                //If Down
                else if(k==3){
                    //If down border
                    if(i == ligne-1){
                        matrice_case.push_back(false);
                    }
                    //We need at least two open paths
                    if(counter_openpath <= 1){
                        matrice_case.push_back(true);
                        counter_openpath += 1;
                    }
                    else{
                        matrice_case.push_back((bool)random);
                        counter_openpath += random;
                    }
                }
                std::cout<<"Path n°"<<k<<" at row n°"<<i<<" and column n°"<<j<<" : "<<matrice_case.at(k)<<std::endl;
            }
            matrice_colonne.push_back(matrice_case);
        }
        matrice_terrain.push_back(matrice_colonne);
    }

    int value_start = 2+rand()%2;
    FlyingCar car = FlyingCar(0,0,value_start, matrice_terrain);


    GLfloat time_start = glfwGetTime();

    float cycle_duration=40.0f;

    std::vector<int> previous_position = {0, 0};
    // The main loop of the program
    while (!glfwWindowShouldClose(window))
    {
        // Retrieve events and call the corresponding callback functions
        glfwPollEvents();
        
        // Setting the camera 
        camera.Do_Movement();
        camera.change_camera_mode();

        //sets the time of the day/night 
        GLfloat time = glfwGetTime();
        GLfloat time_circ=fmod(time,cycle_duration);

        //sets the color of the sky and the intensity of the ambient light
        float w = std::max(0.0f,(float)sin(2*3.14159265359*time_circ/cycle_duration));
        glClearColor(w*135.0f/255.0f,w*206.0f/255.0f,w*250.0f/255.0f,10.f);
        glClear(GL_COLOR_BUFFER_BIT);

        float ambientStr=0.3+max(0.0,0.7*sin(2*3.14159265359*time_circ/cycle_duration));
        
        // Inform OpenGL that we want to use the shaders we have created
        shaders.Use();

        if(time - time_start > 1){
            time_start = time;
            previous_position.at(0) = car.position.at(0);
            previous_position.at(1) = car.position.at(1);
            car.move();

        }

        glm::vec3 lPos;
        glm::vec3 lColor; 

        //day mode 
        if(time_circ<cycle_duration/2.0){

            //set the position of the sun in the sky
            GLfloat y = 100*cos(M_PI*time_circ/cycle_duration);
            GLfloat z = 100*sin(M_PI*time_circ/cycle_duration);
            lPos = glm::vec3(0.0f,y,z);

            //white light for the day 
            lColor = glm::vec3(1.0f, 1.0f, 1.0f);
        }
        //night mode
        else{
            lPos = glm::vec3(0.0f,1.0f,0.0f);
            lColor = glm::vec3(1.0f, 1.0f, 1.0f);
        }

        // Recover the identifiers of the global variables of the shader
        GLint modelLoc = glGetUniformLocation(shaders.Program, "model");
        GLint viewLoc  = glGetUniformLocation(shaders.Program, "view");
        GLint projLoc  = glGetUniformLocation(shaders.Program, "projection");
        GLint lightPos = glGetUniformLocation(shaders.Program, "lightPos");
        GLint lightColor = glGetUniformLocation(shaders.Program, "lightColor");
        GLint ambientStrength = glGetUniformLocation(shaders.Program, "ambientStrength");
        GLint viewPosLoc = glGetUniformLocation(shaders.Program, "viewPos");

 
        // Model matrix (translation, rotation and scale)
        glm::mat4 model = glm::mat4(1.0f);
        //model = glm::translate(model, glm::vec3(4*Redvalue, 0.0f, 0.0f));
        //model = glm::rotate(model, glm::radians(-180 * Redvalue), glm::vec3(1.0, 1.0, 0.0));


        // Camera matrix
        glm::mat4 view;
        view = camera.GetViewMatrix();
        // Projection matrix (generate a perspective projection matrix)
        glm::mat4 projection;
        projection = glm::perspective(45.0f, (GLfloat)WIDTH / (GLfloat)HEIGHT, 0.1f, 100.0f);

        // Update the global variables of the shader
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

       //send light position and color to the fragment shader
        glUniform3f(lightColor, lColor.x, lColor.y, lColor.z);
        glUniform3f(lightPos, lPos.x, lPos.y, lPos.z);
        //send ambient strength 
        glUniform1f(ambientStrength,ambientStr);
        //send ambient 
        glUniform3f(viewPosLoc, camera.Position.x, camera.Position.y, camera.Position.z);


        model = glm::mat4(1.0f);
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

        //Draw the buildings according to the terrain matrix
        for(int i = 0; i < ligne; i++){
            for(int j =0; j< colonne; j++){
                std::string tile_type = "";
                for(int k=0; k<4; k++){
                    tile_type = tile_type.append(std::to_string(matrice_terrain.at(i).at(j).at(k)));
                }

                model = glm::mat4(1.0f);
                model = glm::translate(model, glm::vec3(38.6*j, 0.0f, 38.6*i));
                glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                if(tile_type == "0000"){
                    Tile_0000.Draw(shaders);
                }
                if(tile_type == "0001"){
                    Tile_0001.Draw(shaders);
                }
                if(tile_type == "0011"){
                    Tile_0011.Draw(shaders);
                }
                if(tile_type == "0010"){
                    Tile_0010.Draw(shaders);
                }
                if(tile_type == "0111"){
                    Tile_0111.Draw(shaders);
                }
                if(tile_type == "0110"){
                    Tile_0110.Draw(shaders);
                }
                if(tile_type == "0100"){
                    Tile_0100.Draw(shaders);
                }
                if(tile_type == "1111"){
                    Tile_1111.Draw(shaders);
                }
                if(tile_type == "1110"){
                    Tile_1110.Draw(shaders);
                }
                if(tile_type == "1101"){
                    Tile_1101.Draw(shaders);
                }
                if(tile_type == "1100"){
                    Tile_1100.Draw(shaders);
                }
                if(tile_type == "1011"){
                    Tile_1011.Draw(shaders);
                }
                if(tile_type == "1001"){
                    Tile_1001.Draw(shaders);
                }
                if(tile_type == "1010"){
                    Tile_1010.Draw(shaders);
                }
                if(tile_type == "1000"){
                    Tile_1000.Draw(shaders);
                }
                if(tile_type == "0101"){
                    Tile_0101.Draw(shaders);
                }

            }
        }



        //draws the flying vehicle
        model = glm::mat4(1.0f);
        //Negative if the car goes left, positive if the car goes right
        int sign_x = car.position.at(0) - previous_position.at(0) ;
        //Negative if the car goes up, positive if the car goes down
        int sign_y = car.position.at(1) - previous_position.at(1) ;
        model = glm::translate(model, glm::vec3(39 *previous_position.at(1) + 39*sign_y*(time-time_start),
                                                15.0f,
                                                39 *previous_position.at(0) + 39*sign_x*(time-time_start)));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        //Draw the car according to its direction
        if(sign_x<0){
            car_up.Draw(shaders);
        }
        if(sign_x>0){
            car_down.Draw(shaders);
        }
        if(sign_y>0){
            car_right.Draw(shaders);
        }
        if(sign_y<0){
            car_left.Draw(shaders);
        }

        // Exchange rendering buffers to update the window with new content
        glfwSwapBuffers(window);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
    

    // Delete the objects and buffer that we created earlier
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
    
    // End of the program, we clean up the context created by the GLFW
    glfwTerminate();
    return 0;
}

void deplacement(){

}
