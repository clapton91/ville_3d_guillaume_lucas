#include "flyingcar.h"

/**
 * @brief Update the car position according to the terrain
 */
void FlyingCar::move()
{
    //Initial position
    int position_tile = position.at(2);
    // ---Moving car to a new tile according to the current position---
    if(position_tile == 0 || position_tile == 3){
        int sign = (position_tile-1) / (abs(position_tile-1));
        //if sign negative go up, else go down
        position = {position.at(0)+sign, position.at(1), 3-position_tile};
    }
    if(position_tile == 1 || position_tile == 2){
        int sign = (position_tile-1.5) / (abs(position_tile-1.5));
        //if sign negative go right, else go left
        position = {position.at(0), position.at(1)+sign, 3-position_tile};
    }
    // ---Car moved on a new tile---
    std::vector<bool> tile = terrain.at(position.at(0)).at(position.at(1));
    std::vector<int> choice = {0,1,2,3};
    int counter = 4;
    //Set random movement
    srand(clock());
    int random_path = rand()%counter;
    int hypothetical_path = choice.at(random_path);
    //Check if the random movement is correct
    while(tile.at(hypothetical_path) == false || hypothetical_path == position.at(2)){

        choice.erase(choice.begin() + random_path);
        counter -= 1;
        //If all paths are wrong, the car goes back
        if(counter == 0){
            hypothetical_path = position.at(2);
            break;
        }
        random_path = rand()%counter;
        hypothetical_path = choice.at(random_path);
    }
    position.at(2) = hypothetical_path;


}
