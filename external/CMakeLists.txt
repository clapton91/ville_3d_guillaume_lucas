if(MSVC AND NOT "${MSVC_VERSION}" LESS 1400)
	add_definitions( "/MP" )
endif()

add_definitions(
	-DTW_STATIC
	-DTW_NO_LIB_PRAGMA
	-DTW_NO_DIRECT3D
	-DGLEW_STATIC
	-D_CRT_SECURE_NO_WARNINGS
)

#GLFW
add_subdirectory (glfw-3.3)

include_directories(
	glfw-3.3/include/GLFW/
	glew-2.1.0/include/
)

if(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
set(OPENGL_LIBRARY
	${OPENGL_LIBRARY}
	-lGL -lGLU -lXrandr -lXext -lX11 -lrt
	${CMAKE_DL_LIBS}
	${GLFW_LIBRARIES}
)
elseif(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
set(OPENGL_LIBRARY
	${OPENGL_LIBRARY}
	${CMAKE_DL_LIBS}
	${GLFW_LIBRARIES}
)
endif(${CMAKE_SYSTEM_NAME} MATCHES "Linux")

#GLEW
set(GLEW_SOURCE
	glew-2.1.0/src/glew.c
)

set( GLEW_HEADERS
)

add_library( GLEW_210 STATIC
	${GLEW_SOURCE}
	${GLEW_INCLUDE}
)

target_link_libraries( GLEW_210
	${OPENGL_LIBRARY}
	${EXTRA_LIBS}
)

#SOIL
set( SOIL_SOURCE
	soil/src/image_helper.c
    soil/src/stb_image_aug.c
    soil/src/image_DXT.c
	soil/src/SOIL.c
)

set( SOIL_HEADERS
	soil/src/SOIL.h
)

add_library( SOIL STATIC
	${SOIL_SOURCE}
	${SOIL_INCLUDE}
)

target_link_libraries( SOIL )

#ASSIMP
set(ZLIB_LIBRARIES zlibstatic)
set(ENABLE_BOOST_WORKAROUND ON)
set(BUILD_STATIC_LIB ON)
set(BUILD_ASSIMP_TOOLS  ON)
set(ASSIMP_BUILD_STATIC_LIB ON)

add_subdirectory(assimp-5.0.0)